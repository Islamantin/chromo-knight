﻿using UnityEngine;
using System.Collections;

public class Background : MovingObject {
	public GameObject enemyPrefab;
	public GameObject[] trophiesPool;
	private BackgroundManager backgroundManager;
	private float progress = 0f;
	private bool notified = false;

	void Start () {
        backgroundManager = GameManager.instance.backgroundManager;
        // создаёт противника с 50% вероятностью и при условии, что противник не был создан на предыдущем бэке
        if (backgroundManager != null) {
			if (Random.value < 0.5 && progress<0.5f && GameManager.CurrentGameMode == GameMode.RUNNER) {
				if (enemyPrefab != null) {
					if (enemyPrefab.GetComponent<Enemy> () != null && backgroundManager.EnemyCreationApplied()) {
						GameObject go = (GameObject) Instantiate (enemyPrefab, transform.position, enemyPrefab.transform.rotation);
						go.transform.parent = transform;
						float maxXPos = (GetComponent<SpriteRenderer> ().sprite.texture.width / 2) / GetComponent<SpriteRenderer> ().sprite.pixelsPerUnit - 0.5f;
						float enemyLocalXPosition = ((Random.value * 2) - 1) * maxXPos;
						go.transform.localPosition = new Vector3 (enemyLocalXPosition, -1, 0);
					}
				}
			}
            //если это режим бега, то от 0 до 3 раз вызывается метод создания трофея
			if (GameManager.CurrentGameMode == GameMode.RUNNER) {
				for (int i=0; i<Random.Range(0,4); i++)
				CreateTrophy ();
			}
		}
	}
	
	void Update () {
		if (backgroundManager != null && backgroundManager.start!=null && backgroundManager.finish!=null) {
			if (progress < 1) {
				progress += movingValue;
				transform.position = Vector3.Lerp (backgroundManager.start.position, backgroundManager.finish.position, progress);
                // если этот бэк достиг середины, то опевестить менеджер о том, что нужно создать новый бэк
				if (progress >= 0.5 && !notified) {
					notified = true;
					backgroundManager.MidleOfWayNotify (progress);
//					GetComponent<SpriteRenderer> ().sortingOrder = 0;
				}
			} else {
				Destroy (gameObject);
			}
		}
	}

    //создаёт трофей
	void CreateTrophy(){
        // сперва узнаёт какого типа будет создаваемый трофей
			if (trophiesPool.Length > 0) {
			GameObject trophyToInstantiate = null;
			Trophy.TrophyType targetTrophy = Trophy.GetRandomTrophyType ();
			foreach (GameObject go in trophiesPool){
				if (go.GetComponent<Trophy>().type==targetTrophy)
					trophyToInstantiate = go;
			}
			if (trophyToInstantiate != null) {
				GameObject go = (GameObject)Instantiate (trophyToInstantiate, transform.position, trophyToInstantiate.transform.rotation);
				go.transform.parent = transform;
				float maxXPos = (GetComponent<SpriteRenderer> ().sprite.texture.width / 2) / GetComponent<SpriteRenderer> ().sprite.pixelsPerUnit - 0.5f;
				float maxYPos = (GetComponent<SpriteRenderer> ().sprite.texture.height / 2) / GetComponent<SpriteRenderer> ().sprite.pixelsPerUnit - 0.5f;
				float trophyLocalXPosition = ((Random.value * 2) - 1) * maxXPos;
				float trophyLocalYPosition = ((Random.value * 2) - 1) * maxYPos;
				go.transform.localPosition = new Vector3 (trophyLocalXPosition, trophyLocalYPosition, 0);
			}
		}
	}

    //используется для переноса первого бэка со стартовой точки
	public void SetProgress(float value){
		progress = value;
	}
}

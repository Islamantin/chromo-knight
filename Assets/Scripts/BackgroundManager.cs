﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GameManager))]
public class BackgroundManager : MonoBehaviour {
	public BackgroundPool[] backgroundPools;
	public Transform start;
	private static float xStart;
	public Transform finish;
	private static float xFinish;
	private Background rawBackground;
	public Background RawBackground{ get { return rawBackground; } }
	private BackgroundPool currentPool;
	private GameManager gameManager;
	private bool nextEnemyCreationApplied = true;

	void Start(){
        gameManager = GameManager.instance;
        currentPool = backgroundPools[0];
        //назначает позиции точкам появления\исчезновения бэков по длине спрайта
        Sprite s = backgroundPools[0].backgrounds[0].GetComponent<SpriteRenderer>().sprite;
        float xOffset = (s.texture.width / s.pixelsPerUnit) * 1.375f; //последний множетель взят методом подбора))
        start.position = new Vector3(xOffset, start.position.y, start.position.z);
        finish.position = new Vector3(-xOffset, finish.position.y, finish.position.z);
        xStart = start.position.x;
        xFinish = finish.position.x;
        //создаёт новый бэк и ставит его в центр экрана
        CreateNewBackground().SetProgress(0.5f);
    }

	public static float GetStartAndFinishXDifference(){
		return Mathf.Abs (xFinish - xStart);
	}
	
    //выбирает и создаёт случайный бэкграунд из данного пула
	Background CreateNewBackground(){
        //::добавить систему чередования пулов бэкграундов
		GameObject[] bgs = currentPool.backgrounds;
		GameObject backgroundToInstantiate = bgs [Random.Range (0, bgs.Length)];
		if (backgroundToInstantiate!=null && backgroundToInstantiate.GetComponent<Background> () != null) {
			GameObject go = (GameObject)Instantiate (backgroundToInstantiate, start.position, start.rotation);
			rawBackground = go.GetComponent<Background> ();
			return rawBackground;
		}
		return null;
	}

    //создать новый бэк при достижении центра экрана предыдущим
	public void MidleOfWayNotify(float progressValue){
		if (progressValue >= 0.5) {
			CreateNewBackground ();
		}
	}

    //можно ли на данном бэке создать противника (можно только на послеследующем)
	public bool EnemyCreationApplied(){
		if (nextEnemyCreationApplied) {
			nextEnemyCreationApplied = false;
			return true;
		} else nextEnemyCreationApplied = true;
		return false;
	}
}

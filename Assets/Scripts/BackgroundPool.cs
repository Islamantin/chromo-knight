﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BackgroundPool {
	public GameObject[] backgrounds;
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GameManager))]
public class ClashManager : MonoBehaviour {
    public ColorButton[] buttons;
	public Transform enemyClashTransform;
	private GameManager gameManager;
	private OutputManager outputManager;
	private PlayerStatus player;
	private Enemy currentEnemy;
	private ColorButton selectedButton;
	private Coroutine currentClashingCoroutine;

	void Start(){
		gameManager = GameManager.instance;
		outputManager = gameManager.outputManager;
		player = gameManager.player;
	}

    //оповестить о появлении противника
	public void Notify(Enemy enemy){
		if (enemy!=null) {
			CallToClash (enemy);
        }
	}

	public void CallToClash(Enemy enemy){
		if (enemy != null) {
			currentEnemy = enemy;
            gameManager.ChangeGameMode (GameMode.CLASH);
		}
	}

    //останавливает предыдущее столкновение и начинает новое
	public void StartClash(GameManager gm){
		if (gm == gameManager) {
			StopClash (gm);
			currentClashingCoroutine = StartCoroutine (ProcessClashing ());
		}
	}

	public void StopClash(GameManager gm){
		if (gm == gameManager) {
			if (currentClashingCoroutine != null)
				StopCoroutine (currentClashingCoroutine);
		}
	}
    
    //собственно сам процесс битвы)
	IEnumerator ProcessClashing(){
        //отправляет героя в точку "столкновения"
        outputManager.hero.animator.SetInteger("animID", 1);
        //получаем новый массив цветов для кнопок и показываем их
        SetButtonsColors();
        outputManager.ShowButtons();
        // ждём пока герой не добежит до точки удара
        yield return new WaitUntil(() => outputManager.hero.OnClash == true);
        //скрываем кнопки
        outputManager.HideButtons();
        //запускаем анимацию атаки врага
        currentEnemy.Atack();
        //если игрок выбрал кнопку
        if (selectedButton != null) {
            //если цвет выбранной кнопки правильный
            if (selectedButton.Color == currentEnemy.Color) {
                //герой производит атаку, ждём пока не перейдёт в следующее состояние
                outputManager.hero.PlayAtack(selectedButton.Color);
                yield return new WaitUntil(() => outputManager.hero.animator.GetInteger("animID") == 2);
                //если данной атакой нам удалось убить врага
                if (currentEnemy.TryToKill (player.IsOnFire)) {
                    //даём очки, буст
                    gameManager.soundManager.PlaySound("hit");
                    player.IncreaseBoostLevel(1);
					player.RaiseTheScore (50, currentEnemy.transform.position);
                    //открепляем врага от бэка и переходим в режим быстрой перемотки бэков для создания иллюзии отлетания противника :))
                    if (currentEnemy.transform.parent == null)
						currentEnemy.transform.parent = gameManager.backgroundManager.RawBackground.transform;
                    gameManager.ChangeGameMode (GameMode.FASTROLL);
                    //ждём пока герой не вернётся на место, переходим в обычный режим игры
                    yield return new WaitUntil (() => outputManager.hero.animator.GetInteger("animID")==0);
                    gameManager.ChangeGameMode (GameMode.RUNNER);
				} else {
                    //если противник не убит, поднимаем уровень буста, даём очки, так же быстро перематываем бэк и ждём
                    gameManager.soundManager.PlaySound("hit");
                    player.IncreaseBoostLevel(1);
					player.RaiseTheScore (10, currentEnemy.transform.position);
                    gameManager.ChangeGameMode (GameMode.FASTROLL);
                    yield return new WaitUntil(() => outputManager.hero.animator.GetInteger("animID") == 0);
                    //начинаем всё по-новой
                    CallToClash(currentEnemy);
				}
			} else {
                //если выбрали не правильный цвет, наносим урон и снимаем буст; состояние анимации автоматически перейдёт в "отлетание" от удара противника
                player.ClearBoostEffect ();
				player.DealDamage ();
                gameManager.soundManager.PlaySound("fail");
                currentEnemy.Heal ();
				if (GameManager.CurrentGameMode != GameMode.GAMEOVER) {
                    //ждём и начинаем всё по-новой
                    yield return new WaitUntil(() => outputManager.hero.animator.GetInteger("animID") == 0);
                    CallToClash (currentEnemy);
				}
			}
            selectedButton = null;
		} else {
            //при невыборе кнопки - то же самое, что и при выборе "неправильной"
            player.ClearBoostEffect ();
			player.DealDamage ();
            gameManager.soundManager.PlaySound("fail");
            currentEnemy.Heal ();
			if (GameManager.CurrentGameMode != GameMode.GAMEOVER) {
                yield return new WaitUntil(() => outputManager.hero.animator.GetInteger("animID") == 0);
                CallToClash (currentEnemy);
			}
		}
	}

    //получает от врага массив цветов, перемешивает, назначает кнопкам
    public void SetButtonsColors()
    {
        if (currentEnemy != null)
        {            
            Color[] buttonColors = currentEnemy.PossibleColors;
            for (var i = buttonColors.Length - 1; i > 0; i--)
            {
                var r = Random.Range(0, i);
                var tmp = buttonColors[i];
                buttonColors[i] = buttonColors[r];
                buttonColors[r] = tmp;
            }
            for (int i = 0; i < buttons.Length; i++)
            {
                if (buttons[i] != null)
                {
                    buttons[i].SetColor(buttonColors[i]);
                }
            }
        }
    }

    //назначется кнопка, с которой будет производиться атака
	public void PerformColorAtack(ColorButton button){
		selectedButton = button;
	}
}

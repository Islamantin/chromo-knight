﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorButton : MonoBehaviour {
	public GameManager gameManager;
	public Image image;
	public Sprite pressedSprite;
	public Sprite unpressedSprite;
	private Color currentColor;
	public Color Color{ get { return currentColor; } }

    //назначает цвет данной кнопке
	public void SetColor(Color c){
		currentColor = c;
		image.color = c;
	}

	public void MakePressed(){
		image.sprite = pressedSprite;
	}

	public void MakeUnpressed(){
		image.sprite = unpressedSprite;
	}

    //выбирает данный цвет атаки по нажатию на кнопку
	public void SelectThisColor(){
        gameManager.clashManager.PerformColorAtack (this);
        gameManager.soundManager.PlaySound("button");

    }
}

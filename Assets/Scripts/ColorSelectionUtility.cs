﻿using UnityEngine;
using System.Collections;

public class ColorSelectionUtility: MonoBehaviour {
	public Texture2D colorSpecterTexture;
	private static Color currentTransfusionColor = Color.red;
	private int transfusionCoord;
	private static int transfusionSpeed;

	void Update(){
        //каждый фрейм по координатам на текстуре с определённой скоростью назначается новый цвет, который используется для "переливающихся" спрйтов
        if (colorSpecterTexture!=null) {
			transfusionCoord++;
			currentTransfusionColor = GetColorByCoordinate(transfusionCoord*transfusionSpeed);
		}
	}

    //метод больше не используется

	//public static Color GetRandomPleasantColor(){
	//	float r = 1f; float g = 1f; float b = 1f;
	//	switch (Random.Range (1, 3)) {
	//	case 1:
	//		r = 1f;
	//		g = Random.value;
	//		b = Random.value;
	//		break;
	//	case 2:
	//		r = Random.value;
	//		g = 1f;
	//		b = Random.value;
	//		break;
	//	case 3:
	//		r = Random.value;
	//		g = Random.value;
	//		b = 1f;
	//		break;
	//	}
	//	return new Color (r, g, b);
	//}

	public Color GetColorByCoordinate(int coordinate){
		while (coordinate > colorSpecterTexture.width)
			coordinate -= colorSpecterTexture.width;
		return colorSpecterTexture.GetPixel (coordinate, 0);
	}

    //возвращает цвета расположенные по координатам через каждый step
    public Color[] GetColorsOnEqualGap(int step){
		Color[] resultColors = new Color[4];
		int startCoord = Random.Range (0, colorSpecterTexture.width);
		for (int i = 0; i < resultColors.Length; i++) {
			resultColors [i] = GetColorByCoordinate(startCoord+i*step);
		}
		return resultColors;
	}

    //получить этот "переливающийся" цвет
	public static Color GetCurentTransfusionColor(){
		return currentTransfusionColor;
	}

    //назначить скорость "переливания"
	public static void SetTransfusionSpeed(int speed){
		transfusionSpeed = speed;
	}

}

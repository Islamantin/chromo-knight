﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour {
	public GameObject healthView;
	public GameObject[] healthPointSprites;
	public SpriteRenderer colorSprite;
	public Animator animator;
	private ClashManager clashManager;
	private Color currentColor;
	public Color Color{get{return currentColor;}}
	private Color[] possibleColors;
	public Color[] PossibleColors{ get { return possibleColors; } }
	private Vector3 instancePosition;
	private ColorSelectionUtility colorSelector;
	private int healthPoints;
	private int maxHealth = 3;
	bool notified = false;

	void Awake(){
		colorSelector = (ColorSelectionUtility)FindObjectOfType (typeof(ColorSelectionUtility));
	}

	void Start () {
        clashManager = GameManager.instance.clashManager;
        //назначает случайное количество начального здоровья
        if (clashManager != null) {
			healthPoints = Random.Range (1, maxHealth+1);
			foreach (GameObject go in healthPointSprites)
				go.SetActive (false);
			for (int i = 0; i < healthPoints; i++)
				healthPointSprites [i].SetActive (true);
			GetColors ();
            colorSprite.color = currentColor;
        }
	}

	void Update(){
		if (clashManager != null) {
            //при достижении точки обнаружения противника оповещает clashManager о том, что нужно начать битву
			if (transform.position.x- clashManager.enemyClashTransform.position.x<=0 && !notified){
				notified = true;
                clashManager.Notify (this);
			}
		}
	}

    //получает массив из 4х цветов и выбирает себе один из них
	void GetColors(){
		possibleColors = colorSelector.GetColorsOnEqualGap (200);
		currentColor = possibleColors [Random.Range (0, possibleColors.Length)];
    }
		
	public void Atack(){
        animator.Play("atack");
	}

	public bool TryToKill(bool byBoost){
        //если здоровье упало до нуля или атака была произведена с бустом, убивает этого противника
		if (--healthPoints == 0 || byBoost) {
			animator.Play ("die");
			if (healthView!=null)
			healthView.SetActive(false);
			StartCoroutine (ChangeColor (false));
			return true;
		}
        // иначе наносит урон и вызывает получение новых цветов
        StartCoroutine(ChangeColor(true));
        GetColors();
        animator.Play ("fail");
        transform.parent = null;;
		foreach (GameObject go in healthPointSprites)
			go.SetActive (true);
		for (int i = maxHealth - 1; i >= healthPoints; i--)
			healthPointSprites [i].SetActive (false);
		return false;
	}

    //создаёт эффект получения урона
	IEnumerator ChangeColor(bool byNew){
        Color tempColor = colorSprite.color;
        colorSprite.color = Color.white;
		yield return new WaitForSeconds (0.1f);
		colorSprite.color = tempColor;
		yield return new  WaitForSeconds (0.1f);
		colorSprite.color = Color.white;
		yield return new  WaitForSeconds (0.1f);
		colorSprite.color = tempColor;
		yield return new  WaitForSeconds (0.1f);
		colorSprite.color = Color.white;
		yield return new  WaitForSeconds (0.1f);
        colorSprite.color = currentColor;
    }

    //восстанавливает одно очко здоровья
	public void Heal(){
		if (++healthPoints >= maxHealth) {
			healthPoints = maxHealth;
		}
		foreach (GameObject go in healthPointSprites)
			go.SetActive (false);
		for (int i = 0; i < healthPoints; i++)
			healthPointSprites [i].SetActive (true);
	}

    //используется при для того чтобы "оттащить" труп врага вместе с бэком
	public void SetParent(Transform newParent){
		transform.parent = newParent;
	}
}
	

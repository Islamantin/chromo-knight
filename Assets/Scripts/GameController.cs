﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    //запускает луч по каждому щелчку или тачу, вызывает метод Affect у всех попавших под это действие ITouchAffectable
	void Update () {
		if (GameManager.CurrentGameMode!=GameMode.GAMEOVER)
		if (Input.touches.Length>0 || Input.GetMouseButtonDown(0)){
		Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (pos, Vector2.zero);
			if (hit.collider!=null && hit.collider.GetComponent<ITouchAffectable>() != null) {
				hit.collider.GetComponent<ITouchAffectable> ().Affect();
			}
		}
	}
}

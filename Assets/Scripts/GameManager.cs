﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public float instanceGameSpeed = 0.1f;
	public float accelerationValue = 0.01f;
	public float accelerationCooldown = 5f;
	public ClashManager clashManager;
	public BackgroundManager backgroundManager;
	public OutputManager outputManager;
    public SoundManager soundManager;
    public PlayerStatus player;
    private static float currentGameSpeed;
    public static float CurrentGameSpeed { get { return currentGameSpeed; } }
    private static GameMode currentGameMode = GameMode.MENU;
	public static GameMode CurrentGameMode{get{ return currentGameMode;}}
	private bool accelerated = false;
	private float gameSpeedMultiplier = 1f;
    public static GameManager instance;

	void Awake(){
        instance = this;
		currentGameSpeed = instanceGameSpeed;
	}

	void Update () {
		switch (currentGameMode) {
            //назначает всем движущимся объектам, в частности, бэкграундам различную, в зависимости от режима игры, скорость передвижения
		case GameMode.MENU:
			MovingObject.SetMovingValue (currentGameSpeed * Time.deltaTime*gameSpeedMultiplier);
			break;
		case GameMode.RUNNER:
			if (!accelerated)
				StartCoroutine (AccelerateGame ());
			MovingObject.SetMovingValue (currentGameSpeed * Time.deltaTime*gameSpeedMultiplier);
			break;
		case GameMode.CLASH:
			MovingObject.SetMovingValue (0);
			break;
		case GameMode.FASTROLL:
			MovingObject.SetMovingValue (currentGameSpeed * Time.deltaTime * 2*gameSpeedMultiplier);
			break;
		case GameMode.GAMEOVER:
			MovingObject.SetMovingValue (0);
			break;
		}		
	}

	public void SpeedUpGameBy(float value){
		gameSpeedMultiplier = Mathf.Abs (value);
	}

    //смена режима игры
	public void ChangeGameMode(GameMode newGameMode){
		currentGameMode = newGameMode;
		switch (currentGameMode){
		case GameMode.CLASH:
                clashManager.StartClash (this);
			break;
		case GameMode.GAMEOVER:
				outputManager.ShowGameOverScreen ();
			break;
		}
	}

    //начало игры после нажатия кнопки старта
	public void StartGame(){
		if (currentGameMode == GameMode.MENU && outputManager!=null) {
            soundManager.PlaySound("button");
			outputManager.FadeMenu ();
			currentGameMode = GameMode.RUNNER;
		}
	}

    //возврат в меню после нажатия кнопки рестарта на экране смерти
	public void ToMenu(){
		currentGameMode = GameMode.MENU;
		UnityEngine.SceneManagement.SceneManager.LoadScene ("main");
	}

    //ускорение игры и анимации героя каждые accelerationCooldown секунд
    IEnumerator AccelerateGame(){
		accelerated = true;
		yield return new WaitForSeconds (accelerationCooldown);
        outputManager.hero.SetAnimatorSpeed(1+currentGameSpeed);
        currentGameSpeed += accelerationValue;
		accelerated = false;
	}		
}

public enum GameMode{
	MENU,
	RUNNER,
	CLASH,
	FASTROLL,
	GAMEOVER
}
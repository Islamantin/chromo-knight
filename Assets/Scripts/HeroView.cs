﻿using UnityEngine;
using System.Collections;


public class HeroView : MonoBehaviour {
	public Animator animator;
	public SpriteRenderer swordSprite;
    private bool isOnClash;
    public bool OnClash { get { return isOnClash; }}

	void Update(){
        //пока герой не атакует, меняет цвет меча каждый кадр
		if (swordSprite != null && GameManager.CurrentGameMode!= GameMode.GAMEOVER && animator.GetInteger("animID")!=2)
			swordSprite.color = ColorSelectionUtility.GetCurentTransfusionColor ();
	}

	public void SetAnimatorSpeed(float newSpeed){
		animator.speed = newSpeed;
	}

    //перейти на стейт бега, при его достижении
    public void PlayRunning()
    {
        animator.SetInteger("animID", 0);
    }

    //отмечает, что герой достиг точки удара по монстру и наоборот
    public void SetOnClash(int value)
    {
        if (value == 0)
            isOnClash = false;
        else if (value == 1)
            isOnClash = true;
    }

    //анимация атаки + цвет меча меняется на цвет атаки
    public void PlayAtack(Color atackColor)
    {
        animator.SetInteger("animID", 2);
        swordSprite.color = atackColor;
    }


    public void PlayeDeath()
    {
        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        GameManager manager = GameManager.instance;
        manager.soundManager.PlaySound("death");
        yield return new WaitUntil(() => animator.GetInteger("animID") == 0);
        animator.SetInteger("animID", -1);
    }
}

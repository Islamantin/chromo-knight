﻿using UnityEngine;
using System.Collections;

public interface ITouchAffectable {
	void Affect();
}

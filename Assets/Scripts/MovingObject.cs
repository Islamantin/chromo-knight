﻿using UnityEngine;
using System.Collections;

public abstract class MovingObject : MonoBehaviour {
	protected static float movingValue;

	public static void SetMovingValue(float value){
		movingValue = value;
	}
}

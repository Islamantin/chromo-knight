﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(GameManager))]
public class OutputManager : MonoBehaviour {
    public HeroView hero;
    public Text coreGameScoreOutput;
	public Text gameOverScoreOutput;
	public GameObject[] healthBar;
	public GameObject[] boostBar;
	public GameObject[] buttons;
	public GameObject buttonsPanel;
	public GameObject coreGameUI;
	public GameObject gameOverUI;
	public GameObject menuUI;
	public GameObject restartButton;
    public GameObject scoreObjectPrefab;
    public Animator menuUIAnimator;
	public Animator coreGameUIAnimator;
	public Animator cameraAnimator;
	public Animator doorsAnimator;

	void Start(){
		coreGameScoreOutput.text = "0";
		gameOverScoreOutput.text = "0";
	}

	void Update(){
        //каждый кадр меняет цвет буст баров
		foreach (GameObject go in boostBar)
			if (go != null)
				go.GetComponent<Image> ().color = ColorSelectionUtility.GetCurentTransfusionColor ();		
			
	}

    //включает буст бары по уровню заряда буста
	public void ShowBoostLevel(int boostLevel){
		foreach (GameObject go in boostBar)
			go.SetActive (false);
		if (boostBar.Length>0)
		for (int i = 0; i < boostLevel; i++) {
			if (boostBar [i]!=null)
			boostBar [i].SetActive (true);
		}
	}

    //включает бары здоровья
	public void ShowHealthLevel(int healthLevel){
		foreach (GameObject go in healthBar)
			go.SetActive (false);
		if (healthBar.Length>0)
			for (int i = 0; i < healthLevel; i++) {
				if (healthBar [i]!=null)
					healthBar [i].SetActive (true);
			}
	}

    //выводит число полученных очков
    public void ShowScoreRising(int raisedScore, int raisingValue, Vector3 scoreObjectPosition)
    {
        coreGameScoreOutput.text = raisedScore.ToString();
        gameOverScoreOutput.text = raisedScore.ToString();
        if (scoreObjectPrefab != null)
        {
            GameObject score = (GameObject)Instantiate(scoreObjectPrefab, scoreObjectPosition, Quaternion.identity);
            score.GetComponent<ScoreScript>().score = raisingValue;
            score.transform.position = new Vector3(score.transform.position.x, score.transform.position.y, -2);
        }
    }

    //запускает анимацию скрытия меню
    public void FadeMenu(){
		if (menuUIAnimator != null && menuUI != null) {
			StartCoroutine (ProcessMenuFading ());
		}
	}

    //открывает кнопки в бою
	public void ShowButtons(){
		foreach (GameObject b in buttons) {
			b.GetComponent<Button> ().interactable = true;
			b.GetComponent<ColorButton> ().MakeUnpressed ();
		}
		doorsAnimator.Play ("open");
	}

    //закрывает кнопки в бою
	public void HideButtons(){
		foreach (GameObject b in buttons) {
			b.GetComponent<Button> ().interactable = false;
			b.GetComponent<ColorButton> ().MakePressed ();
		}
		doorsAnimator.Play ("close");
	}

    //показывает экран проигрыша
	public void ShowGameOverScreen(){
		StartCoroutine (ProcessGameOverScreenUnfading ());
	}

	IEnumerator ProcessGameOverScreenUnfading(){
		gameOverUI.SetActive (true);
		Color screenColor = gameOverUI.GetComponent<Image> ().color;
		float progress = 0;
        cameraAnimator.Play("hide");
        coreGameUIAnimator.Play("hide");
        while (progress<1){
			progress += Time.deltaTime;
			gameOverUI.GetComponent<Image> ().color = new Color(screenColor.r,screenColor.g,screenColor.b,progress);
			yield return null;
		}
		gameOverUI.GetComponent<Image> ().color = new Color(screenColor.r,screenColor.g,screenColor.b,1);
        yield return new WaitForSeconds(1f);
        coreGameUI.SetActive(false);
        restartButton.SetActive (true);
	}

	IEnumerator ProcessMenuFading(){
		menuUIAnimator.Play ("fade");
		yield return new WaitForSeconds (0.5f);
		menuUI.SetActive (false);
		coreGameUI.SetActive (true);
		cameraAnimator.Play ("show");
		coreGameUIAnimator.Play ("show");
	}

}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GameManager))]
public class PlayerStatus : MonoBehaviour {
	private GameManager gameManager;
	private int playersScore = 0;
	private int maxHealth = 5;
	private int currentHealth;
	private int maxBoostLevel = 5;
	private int currentBoostLevel = 0;
	private bool isOnFire;
	public bool IsOnFire{ get { return isOnFire; } }

	void Start () {
		gameManager = GameManager.instance;
		currentHealth = 5;
		IncreaseBoostLevel(0);
		ColorSelectionUtility.SetTransfusionSpeed (10);
		Heal ();
	}

    //поднять уровень заряда буста
	public void IncreaseBoostLevel(int increasement){
		currentBoostLevel += increasement;
		if (currentBoostLevel >= maxBoostLevel) {
			currentBoostLevel = maxBoostLevel;
            gameManager.outputManager.hero.animator.SetBool("boosted", true);
            gameManager.SpeedUpGameBy (1.1f);
			ColorSelectionUtility.SetTransfusionSpeed (50);
			isOnFire = true;
		}
		gameManager.outputManager.ShowBoostLevel (currentBoostLevel);
	}

    //сбросить эффект буста
	public void ClearBoostEffect(){
        currentBoostLevel = 0;
        gameManager.outputManager.hero.animator.SetBool("boosted", false);
        gameManager.SpeedUpGameBy(1f);
        gameManager.outputManager.ShowBoostLevel(currentBoostLevel);
        ColorSelectionUtility.SetTransfusionSpeed(10);
        isOnFire = false;
    }

    //нанести единицу урона и убить при достижении нуля
    public void DealDamage(){
		currentHealth--;
        gameManager.outputManager.ShowHealthLevel (currentHealth);
		if (currentHealth <= 0) {
            gameManager.outputManager.hero.PlayeDeath();
			gameManager.ChangeGameMode (GameMode.GAMEOVER);
		}
	}
    
    //восстановить единицу здоровья
	public void Heal(){
		if (++currentHealth >= maxHealth)
			currentHealth = maxHealth;
        gameManager.outputManager.ShowHealthLevel (currentHealth);
	}

    //поднять количество очков и показать их
	public void RaiseTheScore(int raisingValue, Vector3 scorePosition){
		playersScore += raisingValue;
        gameManager.outputManager.ShowScoreRising(playersScore, raisingValue, scorePosition);
	}
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (TextMesh))]
[RequireComponent(typeof(Transform))]
public class ScoreScript: MonoBehaviour {

    public int score = 0;
    public float hight;
    public float speed;
    private float startPosition;
    

	// Use this for initialization
	void Start () {
        TextMesh textMesh = gameObject.GetComponent(typeof(TextMesh)) as TextMesh;
        textMesh.text   = "+" + score;
        speed = hight/30;
        startPosition = gameObject.transform.position.y;
		GetComponent<MeshRenderer> ().sortingOrder = 2;        
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.transform.position.y < startPosition + hight)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + speed, gameObject.transform.position.z);
            
        }
        else
        {
            Destroy(gameObject);
        }

	}
}

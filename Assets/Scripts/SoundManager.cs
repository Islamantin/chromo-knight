﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(GameManager))]
public class SoundManager : MonoBehaviour {
    [SerializeField]
    public SoundReference[] soundPool;
    public AudioSource soundSource;
    public AudioClip mainTheme;
    public AudioSource musicSource;

    void Start()
    {
        if (musicSource != null)
        {
            musicSource.clip = mainTheme;
            musicSource.Play();
        }
    }

	public void PlaySound(string key)
    {
        foreach (SoundReference sound in soundPool)
            if (sound.key == key && soundSource!=null) {
                soundSource.clip = sound.clip;
                soundSource.Play();
                return;
            }
    }
}

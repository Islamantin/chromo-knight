﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct SoundReference {
    public string key;
    public AudioClip clip;
}

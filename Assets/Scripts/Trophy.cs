﻿using UnityEngine;
using System.Collections;

public class Trophy : MonoBehaviour, ITouchAffectable {
	public TrophyType type;
    public GameObject bombDestroySprite;
    private GameManager gameManager;
    private bool affected = false;


    void Awake(){
        gameManager = GameManager.instance;
	}

	void Update(){
		if (GetComponent<SpriteRenderer> () != null && type==TrophyType.BOOST)
			GetComponent<SpriteRenderer> ().color = ColorSelectionUtility.GetCurentTransfusionColor ();
	}

    //при попадании тачем или кликом вызывается метод выдачи награды
	void ITouchAffectable.Affect ()
	{
        if (!affected)
        {
            affected = true;
            GiveATrophy();
        }
	}

    IEnumerator BlowTheBomb()
    {
        GameObject bomb = (GameObject) Instantiate(bombDestroySprite, transform.position, Quaternion.identity);
        bomb.transform.parent = transform;
        yield return new WaitForSeconds(0.1f);
        Destroy(bomb);
        Destroy(gameObject);
    }

	void GiveATrophy(){
		if (gameManager.player != null){
			switch (type){
			case TrophyType.SCORE:
                    //выдать очки
                    gameManager.player.RaiseTheScore (25, transform.position);
                    gameManager.soundManager.PlaySound("coin");
                break;
			case TrophyType.HEAL:
                    //вылечить единицу здоровья игроку
                    gameManager.player.Heal ();
                    gameManager.soundManager.PlaySound("potion");
                    break;
			case TrophyType.DAMAGE:
                    //нанести единицу урону игроку
                    gameManager.player.DealDamage ();
                    gameManager.soundManager.PlaySound("bomb");
                    break;
			case TrophyType.BOOST:
                    //перевести в буст-режим
                    gameManager.player.IncreaseBoostLevel (5);
                    gameManager.soundManager.PlaySound("booster");
                    break;
			}
            //если это не бомба, то просто стереть объект, иначе запустить корутину с созданием спрайта взрыва и ожиданием
            if (bombDestroySprite != null && type== TrophyType.DAMAGE)
                StartCoroutine(BlowTheBomb());
            else Destroy (gameObject);
		}
	}

    //реализация системы разной частоты появления трофеев
	public static TrophyType GetRandomTrophyType(){
		var rnd = Random.value;
		if (rnd >= 0.4f && rnd < 0.65f)
			return TrophyType.HEAL;
		else if (rnd >= 0.65f && rnd < 0.9f)
			return TrophyType.DAMAGE;
		else if (rnd >= 0.9f && rnd <= 1f)
			return TrophyType.BOOST;
		return TrophyType.SCORE;
	}

	public enum TrophyType{
		SCORE,
		HEAL,
		DAMAGE,
		BOOST
	}
}

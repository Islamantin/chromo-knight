ChromoKnight - simple mobile 2D runner on Unity
====================

This project was made in October of 2016 for 10 days as a part of practicle work in Digital Media Lab by students of ITIS High School

* Islam Antin (programmer) - islamantin@gmail.com

* Leonid Yuskevich (art & game design) - washyforest@gmail.com

Since passing of practical work project barely was modified and there is many flaws and bugs remained in their places, 
but you can be sure that we have grown in our skills from that time :D

# Game Details
You play as a chromo-knight - the hero with a magical chromo-sword running through the chromo-castle filled by chromo-monsters.
Game is endless, so players objective there is to recive as many points as possible.
But over time it becomes more difficult because the speed of game gradually increases.

While you running, you can pick up various bonuses appearing on background by taping on them.

* Coin - additional score points.

* Bottle - +1 health point.

* Bomb - (antibonus) -1 health point.

* Chromo-ball - maximum chromo-charge.

When you facing a monster, you must choose one of the 4 colors, which most closely matches the opponent's color, to deal damage.
Without making a choice on time or choosing the wrong color, the player takes damage.
Each hit on the enemy increases the chromo-charge. The maximum charge allows you to kill opponents from one hit,
but the only fault resets the counter to zero and restores one unit of health to the enemy.